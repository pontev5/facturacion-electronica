package conexion;

import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import conexion.ObtenerDatosFacturasOracle;

public class EjecucionRepetitiva implements Runnable {

    public void run() {
    	try {
			ObtenerDatosFacturasOracle.main(null);
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
    }
 
    public static void main(String[] args) {
        ScheduledExecutorService scheduler
                            = Executors.newSingleThreadScheduledExecutor();
 
 
        Runnable task = new EjecucionRepetitiva();
        int initialDelay = 1;
        int periodicDelay = 2;
 
        scheduler.scheduleAtFixedRate(task, initialDelay, periodicDelay,
                                                        TimeUnit.MINUTES);
    }
}
