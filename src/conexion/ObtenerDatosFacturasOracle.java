/**
 * 
 */
package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.CallableStatement;

/**
 * @author Luis Cabral
 *
 */
public class ObtenerDatosFacturasOracle {
	private static Connection conexion;

	public static void main(String args[]) throws SQLException {
		run();
	}
	
	static void run() {
		boolean seguir = false;
		seguir = crearConexionOracle();

		if (seguir) {
			seguir = false;
			try {

				seguir = cargaDatosFacturas(conexion);

				if (seguir) {
					obtenerDatosFacturas(conexion);
				} else {
					System.out.println("Error en la carga de datos");
				}

			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			} finally {
				System.out.println("");
				System.out.println("Cerrando conexion");
				try {
					conexion.close();
				} catch (SQLException ex) {
					// TODO Auto-generated catch block
					System.out.println("Error en la base de datos, motivo: " + ex.getMessage());
				}
			}
		}
	}

	static boolean crearConexionOracle() {
		boolean retorno = true;
		try {
			System.out.println("Creando conexion");
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conexion = DriverManager.getConnection("jdbc:oracle:thin:@192.168.150.6:1521:DESA", "prod", "viejito22");
			System.out.println("");
		} catch (SQLException | ClassNotFoundException ex) {
			retorno = false;
			System.out.println("Error en la conexi�n de la base de datos, motivo: " + ex.getMessage());
		}

		return retorno;
	}

	static void obtenerDatosFacturas(Connection conexion) {
		try {
			Statement stmt = conexion.createStatement();
			conexion.setAutoCommit(false);
			ResultSet rset = stmt.executeQuery("select * from W_FACTURACION_ELECTRONICA");
			while (rset.next()) {
				System.out.println(rset.getString(1) + ", " + rset.getString(2) + ", " + rset.getString(3) + ", "
						+ rset.getString(4) + ", " + rset.getString(5) + ", " + rset.getString(6) + ", "
						+ rset.getString(7));
			}
		} catch (SQLException ex) {
			System.out.println("Error en la consulta a la base de datos, motivo: " + ex.getMessage());
		}
	}

	static boolean cargaDatosFacturas(Connection conexion) throws SQLException {
		boolean retorno = true;
		CallableStatement cst = null;
		// Llamada al procedimiento almacenado
		try {
			cst = conexion.prepareCall("{call proc_retorna_datos_factura ()}");
		} catch (SQLException ex) {
			retorno = false;
			// TODO Auto-generated catch block
			System.out.println("Error en la base de datos, motivo: " + ex.getMessage());
		}
		// Ejecuta el procedimiento almacenado
		try {
			cst.execute();
		} catch (SQLException ex) {
			retorno = false;
			// TODO Auto-generated catch block
			System.out.println("Error en la base de datos, motivo: " + ex.getMessage());
		}

		return retorno;
	}
}
